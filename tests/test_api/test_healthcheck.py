from app.db.async_mongodb import AsyncConnectionMongoDB, get_async_connection


def test_healthcheck_success(sync_client):
    response = sync_client.get("/healthcheck")
    assert response.status_code == 200
    assert response.json() == {'app_status': 'App is up and running', 'db_status': 'Connected to database!'}


def test_healthcheck_failure(sync_client):
    sync_client.app.dependency_overrides[get_async_connection] = lambda: AsyncConnectionMongoDB()
    response = sync_client.get("/healthcheck")
    assert response.status_code == 503
    assert response.json() == {'app_status': 'App is up and running', "db_status": "Failed to connect to database"}
