import asyncio

import pytest
import pytest_asyncio
from fastapi.testclient import TestClient
from httpx import AsyncClient

from app.main import app


@pytest.fixture(scope='session', autouse=True)
def sync_client():
    with TestClient(app) as client:
        yield client


@pytest_asyncio.fixture(scope='session', autouse=True)
async def async_client():
    async with AsyncClient(app=app) as client:
        yield client


@pytest.fixture(scope='session', autouse=True)
def db_clean_up():
    yield
    from app.db.sync_mongodb import get_sync_connection, close_sync_mongo_connection
    from app.settings import MONGODB_DB_NAME
    conn = get_sync_connection()
    conn.client.drop_database(MONGODB_DB_NAME)
    close_sync_mongo_connection()
