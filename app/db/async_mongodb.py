import logging

from beanie import init_beanie
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase

from app.settings import MONGODB_URI, MONGODB_DB_NAME

debug_logger = logging.getLogger('debug-logger')


class AsyncConnectionMongoDB:
    client: AsyncIOMotorClient = None
    is_ready: bool = False
    info: str = "Failed to connect to database"


async_connection = AsyncConnectionMongoDB()


async def get_async_connection() -> AsyncConnectionMongoDB:
    """Get or create pool async connections to mongodb"""
    if not async_connection.is_ready:
        await connect_to_async_mongo()
    return async_connection


async def get_current_database() -> AsyncIOMotorDatabase:
    """Returns the database installed via settings"""
    return async_connection.client[MONGODB_DB_NAME]


async def connect_to_async_mongo():
    client = AsyncIOMotorClient(MONGODB_URI, serverSelectionTimeoutMS=5000)
    try:
        await client.server_info()
        await init_beanie(
            database=client[MONGODB_DB_NAME],
            document_models=[
                "app.models.events.Event",
            ],
        )
        debug_logger.info(f"Connected to MongoDB at {MONGODB_URI}")
        async_connection.client, async_connection.is_ready, async_connection.info = client, True, "Connected to database!"
    except Exception as e:
        debug_logger.error(e)


async def close_async_mongo_connection():
    async_connection.client.close()
    debug_logger.info('Connection to MongoDB closed successfully!')


