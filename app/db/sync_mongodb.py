import logging

from pymongo import MongoClient

from app.settings import MONGODB_URI

debug_logger = logging.getLogger('debug-logger')


class SyncConnectionMongoDB:
    client: MongoClient = None
    is_ready: bool = False
    info: str = "Failed to connect to database"


sync_connection = SyncConnectionMongoDB()


def get_sync_connection() -> SyncConnectionMongoDB:
    """Get or create pool sync connections to mongodb"""
    if not sync_connection.is_ready:
        connect_to_sync_mongo()
    return sync_connection


def connect_to_sync_mongo():
    client = MongoClient(MONGODB_URI, serverSelectionTimeoutMS=5000)
    try:
        client.server_info()
    except Exception as e:
        debug_logger.error(e)
    else:
        debug_logger.info(f"Connected to MongoDB at {MONGODB_URI}")
        sync_connection.client, sync_connection.is_ready, sync_connection.info = client, True, "Connected to database!"


def close_sync_mongo_connection():
    sync_connection.client.close()
    debug_logger.info('Connection to MongoDB closed successfully!')
