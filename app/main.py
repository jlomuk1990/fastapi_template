from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI, Depends
from logging.config import dictConfig

from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import Response

from app.api.api import router as api_router
from app.db.async_mongodb import connect_to_async_mongo, close_async_mongo_connection, get_async_connection, \
    AsyncConnectionMongoDB
from app.schemas.healthcheck import HealthcheckResponse

from app.settings import LOG_CONFIG, ALLOWED_HOSTS

dictConfig(LOG_CONFIG)


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Load with start app
    await connect_to_async_mongo()
    yield
    # Clean up with shutdown app
    await close_async_mongo_connection()


app = FastAPI(
    title="Template FastAPI with MongoDB",
    version="0.0.1",
    docs_url="/api/swagger",
    redoc_url=None,
    openapi_url="/api/openapi.json",
    lifespan=lifespan

)

app.add_middleware(
    CORSMiddleware,
    allow_origins=ALLOWED_HOSTS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Include root router v1
app.include_router(api_router, prefix="/v1")


@app.get("/healthcheck", tags=["Healthcheck"], status_code=200, response_model=HealthcheckResponse)
async def healthcheck(response: Response, connection: AsyncConnectionMongoDB = Depends(get_async_connection)):
    if not connection.is_ready:
        response.status_code = 503
    return {"db_status": connection.info}


if __name__ == "__main__":
    uvicorn.run("main:app", reload=True, host="0.0.0.0", port=8000)
