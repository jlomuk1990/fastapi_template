from beanie import Document


class Event(Document):
    id: int
    user_id: int
    status: str
    time: int
    log: str

    class Settings:
        name = "vp_log"
