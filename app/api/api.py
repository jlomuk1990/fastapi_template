from fastapi import APIRouter

from .v1.events import router as event_router

router = APIRouter()
router.include_router(event_router)
