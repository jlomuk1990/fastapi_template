from fastapi import APIRouter

from app.schemas.events import EventResponse
from app.services.events import get_logs_by_user
from starlette.responses import Response

router = APIRouter()


@router.get(
    "/event_logs",
    tags=["event-logs"],
    response_model=list[EventResponse],
    status_code=200,
    responses={404: {"model": list}}
)
async def user_logs(response: Response, user_id: int):
    logs = await get_logs_by_user(user_id)
    if not logs:
        response.status_code = 404
    return logs
