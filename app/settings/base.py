import os
import pathlib
from dotenv import load_dotenv

BASE_PATH = pathlib.Path(__file__).resolve().parent.parent
load_dotenv(BASE_PATH.parent / ".env")

ALLOWED_HOSTS = os.getenv("ALLOWED_HOSTS").split(",")
if not ALLOWED_HOSTS:
    ALLOWED_HOSTS = ["*"]

LOG_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": "%(levelprefix)s %(asctime)s %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
    "handlers": {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    },
    "loggers": {
        "debug-logger": {"handlers": ["default"], "level": "DEBUG"},
    },
}
