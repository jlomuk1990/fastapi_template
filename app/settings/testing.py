import os

from testcontainers.mongodb import MongoDbContainer

MONGODB_DB_NAME = 'test'
MONGODB_URI = os.getenv('TEST_MONGODB_URI')

if not MONGODB_URI:
    test_mongo_container = MongoDbContainer(image='mongo:3.6')
    test_mongo_container.start()
    MONGODB_URI = test_mongo_container.get_connection_url()
