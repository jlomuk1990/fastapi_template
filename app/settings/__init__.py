from .base import *


APP_SETTINGS = os.getenv('APP_SETTINGS')
if APP_SETTINGS and APP_SETTINGS == 'testing':
    from .testing import *
else:
    from .development import *
