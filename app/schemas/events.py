from pydantic import BaseModel


class EventResponse(BaseModel):
    user_id: int
    time: int
    log: str
