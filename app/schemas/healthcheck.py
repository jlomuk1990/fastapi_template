from pydantic import BaseModel


class HealthcheckResponse(BaseModel):
    app_status: str = "App is up and running"
    db_status: str
