from app.models.events import Event


async def get_logs_by_user(user_id: int) -> list[Event]:
    return await Event.find({"user_id": user_id}, sort="-time").to_list(25)

